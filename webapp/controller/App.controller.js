sap.ui.define([
  "sap/ui/core/mvc/Controller",
  "sap/m/BusyDialog"
], function (Controller, BusyDialog) {
  "use strict";
  return Controller.extend("tutorial.controller.App", {
    onShowPurchases() {
      var oBusyDialog = new BusyDialog({
        title: "Loading Data",
        text: "... now loading products",
        showCancelButton: true,
        close: function(){
          oBusyDialog.close();
        }
      });
      oBusyDialog.open();
    }
  });
});
